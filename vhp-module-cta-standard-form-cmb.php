<?php /** content-modules/cmb/vhp-module-cta-standard-form.php - CTA Standard Form Template */

use iSelect\Config as Config;

add_filter( 'cmb_meta_boxes', function( $meta_boxes ) {
	global $iselect_cmbhelper; //this is the global variable for the CMB helper class (themeroot/inc/)
	
	$cpt_slug = Config::getns( 'vertical-templates', 'content-modules', 'cpt_slug' );
	
	$fields = array();
	$fields[] = $iselect_cmbhelper->metaboxes->colourPicker(	'section_header_color',	'Colour Combination (optional) - Grey is default');

	$meta_boxes[] = array(
		'title' => 'Background Colour',
		'pages' => $cpt_slug,
		'priority' => 'high',
		'fields' => $fields,
	);

	$fields = array();
    $fields[] = array(
    	'id' => 'banner_h2',		
    	'name' => 'Headline',	  		
    	'type' => 'text',		
    	'desc' => ''
    );

	$fields[] = array(
		'id' => 'banner_h2',		
		'name' => 'Sub-headline',		  	
		'type' => 'text',		
		'desc' => ''
	);

	$meta_boxes[] = array(
		'title' => 'Standard Form - Headers',
		'pages' => $cpt_slug,
		'priority' => 'high',
		'fields' => $fields,
	);

	$fields = array();
	$fields[] = array( 
		'id' => 'button',		  
		'name' => 'Button Text',	       
		'type' => 'text',  
		'desc' => '');

	/*$fields[] = array( 
		'id' => 'button_url',	  
		'name' => 'Button URL',	       
		'type' => 'text',  
		'desc' => '');*/

		$meta_boxes[] = array(
		'title'    => 'Standard Form - Button',
		'pages'    => $cpt_slug,
		'priority' => 'high',
		'fields'   => $fields,);
	
	return $meta_boxes;
} );