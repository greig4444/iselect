<div class="header-combo">
    <div class="inner-header-combo">
        <?php if (strlen($h1) > 0) : ?>
            <h1 <?php if (strlen($top_padding) > 0) : ?> style="padding-top:<?php echo $top_padding; ?>px" <?php endif; ?>><?php echo $h1; ?></h1>
        <?php endif; ?>

        <div class="text-content">
            <?php if (strlen($h2) > 0) : ?>
                <h2><?php echo $h2; ?></h2>
            <?php endif; ?>
        </div>

        <div class="select-input">
            <?php if (strlen($button_url) > 0 && $button_url !== "#!/") : ?>
                <?php if ($button_type == 'url') : ?>
                    <a class="start-select-link" href="<?php echo $button_url; ?>"><?php echo strlen($button) > 0 ? $button : 'Start'; ?></a>
                <?php else : ?>
                    <form method="post" name="start-select-form" id="start-select-form" class="start-select-form" action="<?php echo $button_url; ?>" novalidate>
                        <button class="start-select-submit" type="submit"><?php echo strlen($button) > 0 ? $button : 'Start'; ?></button>
                    </form>
                <?php endif; ?>
            <?php else :
                    $action = $button_url == "#!/" ? '' : esc_attr(iSelect\SalesFunnelHelper::endpoint('health'));
                ?>
                <form method="post" name="start-select-form" id="start-select-form" class="start-select-form health" action="<?php echo $action; ?>" novalidate>
                    <input type="hidden" id="sf1_healthClientSession_insuranceTypephc" name="healthClientSession.insuranceType" value="phc" />
                    <input type="hidden" id="customer_suburb" name="healthClientSession.customer.suburb" value="" />
                    <input type="hidden" id="customer_state" name="healthClientSession.customer.state" value="" />
                    <input type="hidden" id="customer_postcode" name="healthClientSession.customer.postcode" value="" />
                    <input type="hidden" id="customer_promotionCode" name="healthClientSession.savedPreference.iselectOfferCode" value="<?php echo esc_attr(isset($_REQUEST['promotioncode']) ? $_REQUEST['promotioncode'] : '') ?>" />
                    <input type="hidden" id="customer_membershipType" name="healthClientSession.membershipType" value="" />

                    <div class="input-container" id="type-select">
                        <div class="select-dropdown">
                            <div class="select-input-toggle">
                                <span class="toggleLabel">
                                    <span class="label">I need cover for&hellip;</span>
                                </span>
                                <svg>
                                    <use class="chevron-down" xlink:href="<?php echo esc_attr(get_stylesheet_directory_uri()) ?>/landing-page-2016/components/section-start/chevron-collection.svg#chevron-down" />
                                    <use class="chevron-up" xlink:href="<?php echo esc_attr(get_stylesheet_directory_uri()) ?>/landing-page-2016/components/section-start/chevron-collection.svg#chevron-up" />
                                </svg>
                            </div>
                            <div class="items">
                                <label>
                                    <input type="radio" name="start-select-item" value="1" required="required">
                                    <span class="label">Just for me - male</span>
                                </label>
                                <label>
                                    <input type="radio" name="start-select-item" value="2">
                                    <span class="label">Just for me - female</span>
                                </label>
                                <label>
                                    <input type="radio" name="start-select-item" value="3">
                                    <span class="label">For me and my partner</span>
                                </label>
                                <label>
                                    <input type="radio" name="start-select-item" value="4">
                                    <span class="label">For my whole family</span>
                                </label>
                                <label>
                                    <input type="radio" name="start-select-item" value="5">
                                    <span class="label">For my family - single parent</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="input-container" id="postcode-select">
                        <div class="input-postcode">
                            <span id="clearbox_btn">X</span>
                            <input type="text" name="postcode" placeholder="Enter postcode or suburb" name="healthClientSession.homeAddress" id="healthClientSession_customer_homeAddress" data-icon="location" autocomplete="off" onkeyup="placeLookup(event);" required="required"><br>
                            <div id="suggestBoxElement" class="suggestBoxElement"></div>
                        </div>
                    </div>
                    <button class="start-select-submit" type="submit" disabled><?php echo $button; ?></button>
                </form>
            <?php endif; ?>
        </div>

        <?php if (strlen($copy) > 0) : ?>
            <div class="additional-text"><?php echo $copy; ?></div>
        <?php endif; ?>

        <p class="prefer-to-call">
            <?php if (strlen($prefercall) > 0) : echo $prefercall;
            else : ?>
                <span><a href="tel:131920">Call 131920</a> or <a href="/customer-scheduler">Schedule a call</a></span>
            <?php endif; ?>
        </p>

    </div><!-- .inner-header-combo -->
</div><!-- .header-combo -->

<?php if ($list_items) :
    $list_items = explode("\n", $list_items); ?>
    <div class="list-content">
        <ul>
            <?php foreach ($list_items as $list_item) : ?>
                <li><?php echo $list_item; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<?php if ($floating_button !== 'off') : ?>
    <?php if ($cta_type == 'cta-to-sales-funnel') : ?>
        <div class="sticky-header-search">
            <div class="select-input">
                <div class="button-content">
                    <div class="sales-funnel">
                        <?php if ($button_type == 'url') : ?>
                            <a class="start-select-link" href="<?php echo esc_attr(strlen($button_url) > 0 ? $button_url : iSelect\SalesFunnelHelper::endpoint('health')); ?>"><?php echo esc_html(strlen($cta) > 0 ? $cta : 'Compare Health Insurance'); ?></a>
                            <img src="<?php echo esc_attr(get_stylesheet_directory_uri()) ?>/assets/images/landing-pages/arrow-right-white.svg" alt="White Arrow Right">
                        <?php else : ?>
                            <form method="get" name="start-select-form" id="start-select-form-cta" class="start-select-form" action="<?php echo esc_attr(strlen($button_url) > 0 ? $button_url : iSelect\SalesFunnelHelper::endpoint('health')); ?>" novalidate>
                                <button class="start-select-submit" type="submit"><?php echo esc_html(strlen($cta) > 0 ? $cta : 'Compare Health Insurance'); ?></button>
                                <img src="<?php echo esc_attr(get_stylesheet_directory_uri()) ?>/assets/images/landing-pages/arrow-right-white.svg" alt="White Arrow Right">
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="sticky-header-search">
            <a href="#" class="sticky-header-search-scroll"><?php echo esc_html(strlen($cta) > 0 ? $cta : 'Compare Health Insurance'); ?>
                <img src="<?php echo esc_attr(get_stylesheet_directory_uri()) ?>/assets/images/landing-pages/arrow-top-white.svg" alt="White Arrow Up">
            </a>
        </div>
    <?php endif; ?>
<?php endif; ?>