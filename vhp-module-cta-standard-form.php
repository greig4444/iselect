<?php
/** Template Name: CTA Standard Form Template */
global $selectedfunnel, $validverticals; //the requirements for this module differs from all others, so using global
$uniqueid = get_the_ID();
$colourcomb = get_post_meta($uniqueid, 'section_header_color', true);
$h1 = get_post_meta( get_the_ID(), 'banner_h1', true );
$h2 = get_post_meta( get_the_ID(), 'banner_h2', true );
$button = get_post_meta( get_the_ID(), 'button', true );
$button_url = get_post_meta( get_the_ID(), 'button_url', true );

  
if (in_array($selectedfunnel, $validverticals)) : ?>
    <section id="vertical_banner_<?php echo $orderid; ?>" data-id="<?php echo $uniqueid; ?>" data-pageid="<?php echo $modpageid; ?>" data-moduleorder="<?php echo $orderid; ?>" class="section-start modular-banner-section start-<?php echo $selectedfunnel;?> <?php echo $colourcomb ?> <?php echo $bg_image_class_desktop ?> <?php echo $bg_image_class_tablet ?> <?php echo $bg_image_class_mobile ?> new-web-v2">
        <div id="compareit_<?php echo $orderid; ?>" class="start-content <?php echo $selectedfunnel;?>-funnel">
            <?php iselect_landing_page_part( "modular-landing-page/sales-funnels/".$selectedfunnel, array(
                    'h1'              => $h1,
                    'h2'              => $h2,
                    'button'          => $button,
                    'button_url'      => $button_url,
                )); ?>
        </div>
    </section>
<?php else : ?>

    <section id="vertical_banner_<?php echo $orderid; ?>" data-id="<?php echo $uniqueid; ?>" data-pageid="<?php echo $modpageid; ?>" data-moduleorder="<?php echo $orderid; ?>" class="section-start modular-banner-section start-generic <?php echo $colourcomb ?> new-web-v2">
        <div id="compareit_<?php echo $orderid; ?>" class="start-content generic-funnel <?php echo $bg_image_class_desktop ?> <?php echo $bg_image_class_tablet ?> <?php echo $bg_image_class_mobile ?>">
            <?php iselect_landing_page_part( "modular-landing-page/sales-funnels/generic", array(
                    'h1'              => $h1,
                    'h2'              => $h2,
                    'button'          => $button,
                    'button_url'      => $button_url,
                ) ); ?>
        </div>
    </section>
<?php endif; ?>


<style type="text/css">
    .prefer-to-call {
        display: none !important;
    }

    .header-combo .inner-header-combo {
    max-width: 1140px;
    margin: auto;
    padding: 40px 20px 50px 20px;
    background: none;
    border-radius: 10px;
}

    .modular-2017 .modular-banner-section.section-start h1 {
        font-size: 24px;    
        letter-spacing: 0.27px; 
        line-height: 30px;  
        text-align: center;
    }

    .modular-2017 .modular-banner-section.section-start h2 {
        font-family: 'Proxima Regular', arial, sans-serif;
        color: #464646;
        font-size: 16px;
        letter-spacing: 0.2px;
        line-height: 22px;
    }

    .vertical-2017 .modular-banner-section.new-web-v2 .header-combo .inner-header-combo {
        background: transparent;
    border-radius: 15px;
}
</style>
